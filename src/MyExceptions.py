class TestFailureError(Exception):
    def __init__(self, message: str) -> None:
        super(TestFailureError, self).__init__(message)
        self.message: str = message

    def __str__(self) -> str:
        return self.message


class InvalidInputError(Exception):
    def __init__(self, message: str = "Invalid Input Provided") -> None:
        super(InvalidInputError, self).__init__(message)
        self.message: str = message

    def __str__(self) -> str:
        return self.message


class AddWithNegativeError(Exception):
    def __init__(self, neg_nums: list) -> None:
        super(AddWithNegativeError, self).__init__()
        self.neg_nums: list = neg_nums

    def __str__(self) -> str:
        return ("negatives not allowed: " + ", ".join(self.neg_nums))
