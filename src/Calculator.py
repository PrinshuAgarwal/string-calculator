""" This module contains the addition operation for the string calculator.

    Usage:
        >>> import Calculator
        >>> Calculator.Add("1,2,3")
        6
"""

import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(PROJECT_ROOT)

import re
from src.MyExceptions import InvalidInputError, AddWithNegativeError


def Add(numbers: str) -> int:
    """ Extracts the delimited integers from an input string and provides a sum of the given integers.
        If any error is encountered, due to invalid input or negative numbers in string or any other,
        exceptions with appropriate message will be raised.

        Args:
            numbers: The string containing integers

        Returns: The sum of the integers
    """

    if not isinstance(numbers, str):
        return TypeError("wrong argument type passed to Add() function. Expects: str")

    delim = ',|\n'      # Default delimiters are: ',' and '\n'
    matched = re.search(r"^\/\/(.|\n)\n", numbers)
    if matched is not None:
        # Optional delimiters are present
        delim = matched.group(1)
        numbers = numbers[matched.end():]       # Strip off the optional delimiter portion

    n = 0
    if numbers == "":
        # If string is empty, return result as 0
        return n

    # Split strings based on the delimiter - If default delimiters then regular expression split is
    # required. However, if it is a single user-provided delimiter, proceed with a string spilt as
    # the delimiter may be a regex metacharacter.
    num_list = (re.split(delim, numbers) if delim == ',|\n' else numbers.split(delim))
    negative_nums = list()      # List to store the negative numbers passed as input
    for num in num_list:
        if num != "" and num[0] == '-' and num[1:].isdigit() and int(num[1:]) != 0:     # -0 is not a negative number, so its an invalid input
            negative_nums.append(num)
        elif num.isdigit():
            n += int(num)
        else:
            raise InvalidInputError

    if negative_nums:
        # The negative numbers list is non empty
        raise AddWithNegativeError(negative_nums)

    return n
