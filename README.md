# string-calculator

String Calculator is a simple calcuator which extracts a list of intergers from a string delimited by defined characters, and returns the sum of those integers.

By default, the integers can be delimited using ',' or newline. The user may choose to define a delimiter by providing the optional delimiter portion at the beginning of the string.

### Syntax for input string
```
"//[delimiter]\n[numbers…]"
```
&nbsp;

## Usage
```bash
# You may use the "launch_string_calculator" script to run the string calculator
# This script is located under prohect_scripts directory in this repo.

$ ./launch_string_calculator
Enter the input string containing delimited integers [Type 'exit' to exit]: //\\n1\5
Result: 6
```

```python
# You may import the Calculator module and run the Add() function
import Calculator

# returns 6
Calculator.Add("1,2,3")

#returns 10
Calculator.Add("//;\n5;5")
```
&nbsp;

## Contributing
Merge requests are always welcome. For any changes, please provide an elaborate description about the same on the merge request created.
