"""
This module contains automated unit tests for the String Calculator.

Use:
    This module will be called to run the test suite via provided project scripts.
"""


import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(PROJECT_ROOT)


from src.Calculator import Add
from src.MyExceptions import TestFailureError, InvalidInputError, AddWithNegativeError
from typing import Any

INVALID_INPUT_ERROR = -1

def checkAdd(num_str: str, expValue: Any) -> None:
    """ An implementation of a test condition to check the Add() method of the string calculator.
        This method throws an exception for any test failure.

        Args:
            num_str:
                The input string to Add() method

            expValue:
                The expected integer value that the Add() method should return.
                A string containing exception message may be passed to this argument when
                negative number exception is expected.

        Returns: None
    """

    test_str = "Test: " + repr(num_str) + " || "
    try:
        actualVal = Add(num_str)
        if expValue < 0:
            # Exception was expected
            raise TestFailureError(test_str + "Expected exceptions were not encountered")
        elif type(actualVal) != int:
            # Integer was not returned by function
            raise TestFailureError(test_str + "An integer was not returned by Add()")
        elif actualVal != expValue:
            # Actual and Expected Value Mismatch
            errMsg = test_str + "Actual and expected value mismatch --- Actual: " + str(actualVal) + " | Expected: " + str(expValue)
            raise TestFailureError(errMsg)

    except InvalidInputError as iie:
        if expValue != INVALID_INPUT_ERROR:
            # Invalid input exception was not expected
            raise TestFailureError(test_str + "Unexpected exception: Invalid Input")
        elif iie.message != "Invalid Input Provided":
            # Wrong exception message returned
            raise TestFailureError(test_str + "Wrong error message for invalid input --- Returned: " + iie.message)

    except AddWithNegativeError as awne:
        if not isinstance(expValue, str):
            # Negative number error not expected
            raise TestFailureError(test_str + "Unexpected exception: Negative number not allowed")
        elif awne.__str__() != ("negatives not allowed: " + expValue):
            # Wrong exception message returned
            raise TestFailureError(test_str + "Wrong message for negative no. error --- Returned: " + awne.__str__())

    except (TestFailureError, Exception) as e:
        raise e


def runTests():
    """ This method contains individual test cases that are executed under a test condition.
    
        Args: None

        Returns: None
    """

    testCases = {
        "" : 0,
        "1": 1,
        "1,2": 3,
        "1\n2,3": 6,
        "1, \n": INVALID_INPUT_ERROR,
        "1, 2": INVALID_INPUT_ERROR,
        "12\n3,4": 19,
        "\n1,2\n1": INVALID_INPUT_ERROR,
        "11\n": INVALID_INPUT_ERROR,
        "2\n04": 6,
        "0,001,000\n000002": 3,
        "//;\n1;2": 3,
        "//:\n0:2": 2,
        "//:\n1": 1,
        "// \n1 1 0": 2,
        "//;\n": 0,
        "//\n\n5\n3": 8,
        "//\n\n3,4": INVALID_INPUT_ERROR,
        "//;\n4;5\n6": INVALID_INPUT_ERROR,
        "//;\n4;5;\n6": INVALID_INPUT_ERROR,
        "//;\n4;;5": INVALID_INPUT_ERROR,
        "//;;\n4;;5": INVALID_INPUT_ERROR,
        "//1\n212": 4,
        "//0\n001": INVALID_INPUT_ERROR,
        "//+\n01+99+10+000001+0000000+4": 115,
        "//{\n1{4{6": 11,
        "//)\n2)76": 78,
        "//-\n9-4-5": 18,
        "//+\n9-4-5": INVALID_INPUT_ERROR,
        "//.\n0.0.0": 0,
        "//\n": INVALID_INPUT_ERROR,
        "5\n-2,-3": "-2, -3",
        "1,-0": INVALID_INPUT_ERROR,
        "0\n1\n-2": "-2",
        "1, -2": INVALID_INPUT_ERROR,
        "//;\n2;-3": "-3",
        "//;\n0;-2;-3": "-2, -3",
        "//-\n2-3--1": INVALID_INPUT_ERROR,
        "///\n2/3": 5,
        "//\\\n1\\5": 6     # Actual input test string: //\\n1\5
    }

    for numString, val in testCases.items():
        checkAdd(numString, val)

    print("ALL TESTS HAVE PASSED!!!")
